Abstract 

This report serves as a means of displaying the similarities as well as differences of which learning and carrying out software projects share. The way this is done is through considering how we can model systems. This is then reflected upon to aid in seeing some of the ways that this knowledge is applicable to both learning and carrying out software projects. This is done to help understand the ideas of software engineering as a whole.

Introduction 

A Product Breakdown Structure (referred to as PBS henceforth) for the degree is made. This is a type of hierarchical model displaying the main components of the degree. 

The model is used to show that some modules are optional, while other have dependencies. And as a baseline, near the beginning of the degree, a CV is included as an appendix.

Following the PBS, a Gantt chart for the degree is made. The times displayed are based on the knowledge that the average 10 credit module requires a work time of around 100 hours. 

As software choice was a factor in creating the two models, a brief justification is made weighing the pros and cons as well as a concise cost-benefit analysis. 

To finalise the report reflections are portrayed. Covering considerations in personal ownership of learning, approaches to handling the uncertainty, the nature of the task itself, as well as what has been learned from it in relation to the concepts of software engineering. 

PBS 

[Degree.mvdx](/uploads/c6afb78f816a9f791d0ef3a9e9847afd/Degree.mvdx)[Degree.pdf](/uploads/6facbfb52a17ea66f58d686ff33beb4f/Degree.pdf)[HarveyCV.docx](/uploads/6d052abd741f0dc1126cf2d01dd0dd7e/HarveyCV.docx)![Degree](/uploads/2550e6962ec11cf1f1b7f685764c935d/Degree.jpg)


GANTT 

[Gantt.mvdx](/uploads/a5ce81250bd11fd509fb52a2f8d51cd6/Gantt.mvdx) [Gantt.pdf](/uploads/0338d9ac0e4abdef92b941aa336f2b6c/Gantt.pdf)![Gantt](/uploads/3bd001c4bd879fa7b592161ff38bc5b9/Gantt.jpg)

My choices for PBS and Gantt software were between the programs Dia and Mindview. 

Firstly, I wanted to weigh ease of use and familiarity. I felt that as I had used Dia in the past, it would be optimal for me to use it, however, upon testing Mindview I found its control to be intuitive and easily found myself having an easier time with it. Dia from a usability standpoint is clunky and rather dated. 

Cost was another important decision. While Mindview is technically a paid application and Dia is free, Mindview allows for a 30-day trial period, which allowed usage of the program if needed, this is less optimal than Dia, though I still believe that Mindview is the better choice. 

The reason I believe this is because of the features Mindiew has. Mindview allowed for quick and easy chart design with hotkeys for adding elements, and also allowed my elements to immediately be converted into gantt format. This, coupled with its simplistic UI made Mindview my software of choice for this project.

Reflections 

The nature of the task allowed for a more thorough analysis of the degree. This better allowed me to judge the time needed for each module. I believe this task is successful in helping to see bigger picture.

This can be related to software engineering, a large project is much more easily handled when split up, for instance aiding in telling which parts have a higher percentage of workload and therefore may be of higher priority. And focusing on Gantt, can help greatly with time management, again helping to take smaller parts of the project to help see the bigger picture, the full project.

More personally, the Task allowed me to see my degree in its entirety and what steps are required to be successful, which I found to be the most help part of this assignment. I personally found creating and designing the models easy, yet enjoyable as it was a more creative way of splitting up a larger problem. The task was difficult however in that using new software to create these models was a challenge to overcome and was difficult to export correctly due to this. I feel this way because I know that I enjoy creating a visual aid when attempting to break down a problem, though I also know I find it difficult to tackle new problems quickly, which I believe is why I found using the software troubling. 
If I were to do this again I would perhaps use more familiar software or practise with the software beforehand. 


Bibliography

"http://www.productbreakdownstructure.com/" - Example PBS

"https://docs.gitlab.com/ee/user/markdown.html#wiki-direct-file-link" - Used to add files

"http://www.uefap.com/writing/genre/reflect.htm" - Example reflective writing

"http://www.reading.ac.uk/progspecs/pdf18/UFCOMPB18.pdf" - Used to retrieve module credit scores and compulsory modules

"http://www.reading.ac.uk/modules/module.aspx?sacyr=1718&school=MPS" - used to find further information on modules


